<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

// Route::auth();

// Authentication Routes...
// Route::post('login', 'Auth\LoginController@login');
// Route::post('logout', 'Auth\LoginController@logout');

// Registration Routes...
Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Route::resource('posts', 'PostController');

Route::group(['prefix' => 'posts'], function(){
    Route::resource('', 'PostController', ['except' => [
        'create', 'update', 'destroy'
    ]]);

    Route::post('get', 'PostController@posts');
    
    Route::group(['prefix' => '{post}'], function(){
        Route::get('', 'PostController@post');
        Route::post('comment', 'PostController@comment');
        Route::get('comments', 'PostController@comments');
        Route::post('vote', 'PostController@vote');
    });
});

Route::group(['prefix' => 'map'], function(){
    Route::resource('', 'MapController', ['except' => [
        'create', 'update', 'destroy'
    ]]);
    
    Route::post('upload', 'User\MapController@upload');
    Route::get('categories', 'User\MapController@categories');
});