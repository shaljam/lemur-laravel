<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Post;
use App\Comment;
use App\Vote;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::with(['location', 'user' => function($query){
            $query->select('id', 'username');
        }])->withCount('comments')->get();

        return $posts;
    }

    public function post(Request $request, Post $post)
    {
        // return $post->with('user')->get()[0];

        return Post::with(['location', 'user' => function($query){
                $query->select('id', 'username');
            }, 'votes' => function($query) use($request){
                $query->where('user_id', $request->user()->id);
            }])->withCount('comments')->where('id', $post->id)->first();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|min:1',
            'content' => 'required|min:1',
            'location.lat' => 'required',
            'location.lng' => 'required',
        ]);

        $post = $request->user()->posts()->create($request->all());

        // $location = App\PostLocation::create($request->all());

        $post->location()->create($request->input('location'));

        return response()->json($post);

        // $post->location()->save($location);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function posts(Request $request)
    {    
        $topRight = $request->input('topRight');
        $bottomLeft = $request->input('bottomLeft');
        $posts = Post::whereHas('location', function($query) use($topRight, $bottomLeft){
            $query->whereBetween('lat', [$bottomLeft['lat'], $topRight['lat']]);
            $query->whereBetween('lng', [$bottomLeft['lng'], $topRight['lng']]);
        })->with(['location', 'user' => function($query){
            $query->select('id', 'username');
        }, 'votes' => function($query) use($request){
            $query->where('user_id', $request->user()->id);
        }])->withCount('comments')->latest()->paginate(10);

        return $posts;
    }

    public function comment(Request $request, Post $post)
    {
        $this->validate($request, [
            'text' => 'required|string|between:1,100',
        ]);

        $comment = new Comment;
        $comment->text = $request->input('text');
        $comment->user()->associate($request->user());
        $comment->post()->associate($post);
        $comment->save();

        return $comment;
    }

    public function comments(Post $post)
    {
        $comments = $post->comments()->with(['user' => function($query){
            $query->select('id', 'username');
        }])->paginate(10);

        return $comments;
    }

    public function vote(Request $request, Post $post)
    {
        $this->validate($request, [
            'direction' => 'required|boolean'
        ]);

        $direction = $request->input('direction');

        $vote = Vote::where('user_id', $request->user()->id)->where('post_id', $post->id)->first();

        if($vote != null)
        {
            $vote_change = -1;
            if($vote->direction)
            {
                $vote_change = 1;
            }

            $vote->delete();

            $post->vote_count -= $vote_change;
            $post->save();

            return Post::with(['location', 'user' => function($query){
                $query->select('id', 'username');
            }, 'votes' => function($query) use($request){
                $query->where('user_id', $request->user()->id);
            }])->withCount('comments')->where('id', $post->id)->first();
        }

        $vote_change = -1;
        if($direction)
        {
            $vote_change = 1;
        }

        $post->vote_count += $vote_change;
        $post->save();

        $vote = new Vote;
        $vote->direction = $request->input('direction');
        $vote->user()->associate($request->user());
        $vote->post()->associate($post);
        $vote->save();

        return Post::with(['location', 'user' => function($query){
            $query->select('id', 'username');
        }, 'votes' => function($query) use($request){
            $query->where('user_id', $request->user()->id);
        }])->withCount('comments')->where('id', $post->id)->first();

        // return $vote->makeHidden(['user', 'post']);
    }
}
