<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostLocation extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'lat', 'lng',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function post()
    {
        return $this->belongsTo('App\Post');
    }

    
}
