<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'content',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Get the location record associated with the post.
     */
    public function location()
    {
        return $this->hasOne('App\PostLocation');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment')->orderBy('id', 'desc');
    }

    public function votes()
    {
        return $this->hasMany('App\Vote');
    }
}
